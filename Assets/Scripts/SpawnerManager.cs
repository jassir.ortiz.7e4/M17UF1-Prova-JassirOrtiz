using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    [SerializeField] GameObject enemyShield;
    [SerializeField] GameObject enemyFragate;
    float speed = 2.0f;
    bool movex;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating(nameof(SpawnEnemies), 1, 1.1f);
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }

    void SpawnEnemies() {
        if (Random.Range(0, 8) == 2)
        {
            GameObject enemyObj = Instantiate(enemyFragate, new Vector3(transform.position.x, transform.position.y + 0.5f, 0), Quaternion.identity);
        }
        else {
            GameObject enemyObj = Instantiate(enemyShield, new Vector3(transform.position.x, transform.position.y + 0.5f, 0), Quaternion.identity);
        }
    }

    void Movement() {
        if (movex)
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
        else
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("MainCamera"))
        {
            if (movex)
            {
                movex = false;
            }
            else
            {
                movex = true;
            }
        }
    }
}
