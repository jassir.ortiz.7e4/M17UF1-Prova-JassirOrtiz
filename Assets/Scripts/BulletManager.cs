using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour
{
    Transform player;

    private void Start()
    {
        player = GameObject.Find("PlayerShip").GetComponent<Transform>();
    }
    private void Update()
    {
        MisilFollowPlayer();
    }

    void MisilFollowPlayer()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, 4.5f * Time.deltaTime);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("MainCamera"))
        {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("Misil"))
        {
            Destroy(this.gameObject);
        }
    }
}
