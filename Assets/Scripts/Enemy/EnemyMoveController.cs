using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveController : MonoBehaviour
{
    [SerializeField] float speed;
    public bool movey;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MovementEnemy();
    }

    void MovementEnemy()
    {
        transform.position += Vector3.down * speed * Time.deltaTime;
    }
}
