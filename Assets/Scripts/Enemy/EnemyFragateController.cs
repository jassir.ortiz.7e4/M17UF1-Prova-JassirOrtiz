using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFragateController : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] GameObject misil;
    [SerializeField] GameObject player;
    public bool movey;
    

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating(nameof(SpawnMisil),0.8f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        MovementEnemy();
    }

    void SpawnMisil() {
        if (Random.Range(0, 4) == 2)
        {
            GameObject misilObject = Instantiate(misil, new Vector3(transform.position.x, transform.position.y , 0), Quaternion.identity);
        }
    }

    void MovementEnemy()
    {
        transform.position += Vector3.down * speed * Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("FireBall"))
        {
            Destroy(this.gameObject);
            GameManager.Instance.score += 5;
        }
    }

    public void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
