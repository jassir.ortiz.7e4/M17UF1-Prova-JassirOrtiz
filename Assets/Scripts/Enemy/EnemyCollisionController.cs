using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollisionController : MonoBehaviour
{
    void Start()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
            GameManager.Instance.score += 5;
        }
        if (collision.gameObject.CompareTag("FireBall"))
        {
            Destroy(this.gameObject);
            GameManager.Instance.score += 5;
        }
    }

    public void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
