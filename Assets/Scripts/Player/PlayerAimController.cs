using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAimController : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private Rigidbody2D gun;
    public Vector3 mouse_pos;
    private Vector3 firePoint;
    private float angle;
    private Rigidbody2D rbBullet;
    Vector2 shootVector;

    void Start()
    {
        rbBullet = GetComponent<Rigidbody2D>();
    }


    private void Update()
    {
        ShootGun();
    }

    void ShootGun()
    {
        if (Input.GetKey(KeyCode.Space))
        { 
            rbBullet = Instantiate(gun, transform.position, Quaternion.identity);
            rbBullet.velocity = Vector2.up * 7;
        }
    }
}
